"use client";
import { useRef, useState } from "react";
import classess from "./image-picker.module.css";
import Image from "next/image";

export default function ImagePicker({ label, name }) {
  const [pickedImage, setPickedImage] = useState();
  const ref = useRef();
  const Handle = () => {
    ref.current.click();
  };
  const HanleImageChange = (event) => {
    const file = event.target.files[0];
    if (!file) {
      setPickedImage(null);
      return;
    }
    const fileReader = new FileReader();
    fileReader.onload = () => {
      setPickedImage(fileReader.result);
    };
    fileReader.readAsDataURL(file);
  };
  return (
    <div className={classess.picker}>
      <label htmlFor="image">{label}</label>
      <div className={classess.controls}>
        <div className={classess.preview}>
          {!pickedImage && <p>No Image picked yet!!!</p>}
          {pickedImage && (
            <Image
              src={pickedImage}
              alt="The Image selected by the user"
              fill
            />
          )}
        </div>
        <input
          className={classess.input}
          type="file"
          id="image"
          accept="image/png, image/jpg, image/jpeg"
          name={name}
          ref={ref}
          onChange={HanleImageChange}
          required
        />
        <button className={classess.button} type="button" onClick={Handle}>
          Pick an Image
        </button>
      </div>
    </div>
  );
}
