"use client";
import MealItem from "./meal-item";
import classess from "./meals-grid.module.css";

export default function MealsGrid({ Meals }) {
  return (
    <ul className={classess.meals}>
      {Meals.map((meal) => (
        <li key={meal.id}>
          <MealItem {...meal} />
        </li>
      ))}
    </ul>
  );
}
