"use client";
import Link from "next/link";
import Logo from "@/assets/logo.png";
import classess from "./main-header.module.css";
import Image from "next/image";
import MainHeaderBackground from "../background/main-header-background";
import NavLink from "./nav-link";

export default function MainHeader() {
  return (
    <>
      <MainHeaderBackground />
      <header className={classess.header}>
        <Link href="/" className={classess.logo}>
          <Image src={Logo} alt="A palte with food on it!" priority />
          Nextlevel Food
        </Link>
        <nav className={classess.nav}>
          <ul>
            <li>
              <NavLink href="/meals">Brows Meals</NavLink>
            </li>
            <li>
              <NavLink href="/community">Foodies Community</NavLink>
            </li>
          </ul>
        </nav>
      </header>
    </>
  );
}
