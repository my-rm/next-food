import Link from "next/link";
import classess from "./page.module.css";
import MealsGrid from "@/components/meals/meals-grid";
import { getMeals } from "@/lib/getMeals";
import { Suspense } from "react";

export const metadata = {
  title: "All Meals",
  description: "Browse the Deicious meals shared by our vibrant community.",
};

async function MealsLoading() {
  const meals = await getMeals();
  return <MealsGrid Meals={meals} />;
}

export default function Meals() {
  return (
    <>
      <header className={classess.header}>
        <h1>
          Delicius meals, created{" "}
          <span className={classess.highlight}>by you</span>
        </h1>
        <p>
          Choose your favorite recipe and cook it yourself, It is easy and fun!
        </p>
        <p className={classess.cta}>
          <Link href="/meals/share">Share your favorite recipe</Link>
        </p>
      </header>
      <main className={classess.main}>
        <Suspense
          fallback={<p className={classess.loading}>Fetching meals ...</p>}
        >
          <MealsLoading />
        </Suspense>
      </main>
    </>
  );
}
