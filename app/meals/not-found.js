export default function NotFOund() {
  return (
    <main className="not-found">
      <h1>Meal Not Found</h1>
      <p>Unfortunately, we could not found the requested Meal!</p>
    </main>
  );
}
