"use server";

import { redirect } from "next/navigation";
import { saveMeal } from "./getMeals";
import { revalidatePath } from "next/cache";

function isInvalidText(text) {
  return !text || text.trim() === "";
}

export async function ShareMeal(preveState, formData) {
  const meal = {
    title: formData.get("title"),
    summary: formData.get("summary"),
    instructions: formData.get("instructions"),
    image: formData.get("image"),
    creator: formData.get("name"),
    creator_email: formData.get("email"),
  };
  if (
    isInvalidText(meal.title) ||
    isInvalidText(meal.summary) ||
    isInvalidText(meal.instructions) ||
    isInvalidText(meal.creator) ||
    isInvalidText(meal.creator_email) ||
    !meal.creator_email.includes("@") ||
    !meal.image ||
    meal.image.size === 0
  ) {
    //throw new Error("Invalid inputs !!!");
    return {
      message: "Invalid inputs !!! ... ",
    };
  }
  console.log(meal);
  await saveMeal(meal);
  // revalidatePath("/meals", "layout");
  // revalidatePath("/", "layout");
  revalidatePath("/meals");
  redirect("/meals");
}
